#include <curses.h>
#include <unistd.h>
#include <stdlib.h>

int main()
{
    WINDOW *ablak;
    ablak=initscr();
    
    int xj=0, xk=0, yj=0, yk=0;
    int mx,my;
    getmaxyx (ablak,mx,my);

    for(;;)
    {

        mvprintw(abs(yj+(mx-yk)),abs(xj+(my-xk)), "O");

        refresh ();
        usleep (100000);
        clear();

        xj = (xj - 1) % my;
        xk = (xk + 1) % my;
        yj = (yj - 1) % mx;
        yk = (yk + 1) % mx;

    }
    return 0;
}

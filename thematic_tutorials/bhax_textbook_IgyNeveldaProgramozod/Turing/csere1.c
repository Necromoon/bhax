#include <stdio.h>

int main()
{
    int x = 15, y = 22, z;
    
    printf("x= %d\ny= %d\n",x,y);

    z = x;
    x = y;
    y = z;

    printf("x= %d\ny= %d\n",x,y);

    y = y - x;
    x = x + y;
    y = x - y;

    printf("x= %d\ny= %d\n",x,y);
    
    return 0;
}